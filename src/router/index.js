import Vue from "vue";
import Router from "vue-router";
import NotFound from "../views/404";
import NProgress from "nprogress";
import "nprogress/nprogress.css";

Vue.use(Router);

// eslint-disable-next-line no-undef
const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/user",
      component: () => import(/* xxxx : user*/ "../layouts/UserLayout"),
      children: [
        {
          path: "/user",
          redirect: "/user/login",
        },
        {
          path: "/user/login",
          name: "login",
          component: () => import(/* xxxx : user*/ "../views/User/Login"),
        },
        {
          path: "/user/register",
          name: "register",
          component: () => import(/* xxxx : user*/ "../views/User/Register"),
        },
      ],
    },
    {
      path: "/",
      // meta: { authority: ["user", "admin"] },
      component: () =>
        import(/* webpackChunkName: "layout" */ "../layouts/BasicLayout"),
      children: [
        // dashboard
        {
          path: "/",
          redirect: "/dashboard/analysis",
        },
        {
          path: "/dashboard",
          name: "dashboard",
          // meta: { icon: "dashboard", title: "仪表盘" },
          component: { render: (h) => h("router-view") },
          children: [
            {
              path: "/dashboard/analysis",
              name: "analysis",
              // meta: { title: "分析页" },
              component: () =>
                import(
                  /* webpackChunkName: "dashboard" */ "../views/Dashboard/Analysis"
                ),
            },
          ],
        },
        // form
        {
          path: "/form",
          name: "form",
          component: { render: (h) => h("router-view") },
          // meta: { icon: "form", title: "表单", authority: ["admin"] },
          children: [
            {
              path: "/form/basic-form",
              name: "basicform",
              meta: { title: "基础表单" },
              component: () =>
                import(
                  /* webpackChunkName: "form" */ "../views/Forms/BasicForm"
                ),
            },
            {
              path: "/form/step-form",
              name: "stepform",
              hideChildrenInMenu: true,
              meta: { title: "分布表单" },
              component: () =>
                import(
                  /* webpackChunkName: "form" */ "../views/Forms/StepForm"
                ),
              children: [
                {
                  path: "/form/step-form",
                  redirect: "/form/step-form/info",
                },
                {
                  path: "/form/step-form/info",
                  name: "info",
                  component: () =>
                    import(
                      /* webpackChunkName: "form" */ "../views/Forms/StepForm/Step1"
                    ),
                },
                {
                  path: "/form/step-form/confirm",
                  name: "confirm",
                  component: () =>
                    import(
                      /* webpackChunkName: "form" */ "../views/Forms/StepForm/Step2"
                    ),
                },
                {
                  path: "/form/step-form/result",
                  name: "result",
                  component: () =>
                    import(
                      /* webpackChunkName: "form" */ "../views/Forms/StepForm/Step3"
                    ),
                },
              ],
            },
          ],
        },
      ],
    },
    {
      path: "*",
      name: "404",
      component: NotFound,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
    },
  ],
});

router.beforeEach((to, form, next) => {
  NProgress.start();
  next();
});

router.afterEach(() => {
  NProgress.done();
});

// const router = new VueRouter({
//   mode: "history",
//   base: process.env.BASE_URL,
//   routes,
// });

export default router;
