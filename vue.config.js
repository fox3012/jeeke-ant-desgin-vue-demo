// module.exports = {
//   css: {
//     loaderOptions: {
//       less: {
//         javascriptEnabled: true,
//       },
//     },
//   },
// };
module.exports = {
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
        },
      },
    },
  },
};
// const { defineConfig } = require("@vue/cli-service");
// module.exports = defineConfig({
//   css: {
//     loaderOptions: {
//       less: {
//         javascriptEnabled: true,
//       },
//     },
//   },
//   transpileDependencies: true,
// });
